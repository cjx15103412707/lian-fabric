package com.fabric.utils;

import com.alibaba.fastjson.JSON;
import com.fabric.po.CommonUse;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AgricultureUtils {

    public  List<CommonUse> toListMap(String json){
        List<Object> list = JSON.parseArray(json);
        if(list ==null) return null;
        List<Map<String,Object>> listw = new ArrayList<Map<String,Object>>();
        for (Object object : list){
            Map<String,Object> ageMap = new HashMap<String,Object>();
            Map <String,Object> ret = (Map<String, Object>) object;//取出list里面的值转为map
            listw.add(ret);
        }

        List<CommonUse> commonUses = new ArrayList<>();
        for(int i = 0;i<listw.size();i++){
            CommonUse commonUse = new CommonUse();
            Map<String, Object> s  = (Map<String, Object>) list.get(i);
            String tid = (String) s.get("TID");
            String ttime = (String) s.get("TTime");
            String IsDelete = (String) s.get("IsDelete");
            Object obj = s.get("THistory");
            commonUse.setTID(tid);
            commonUse.setIsDelete(IsDelete);
            commonUse.setTTime(ttime);
            if(null !=obj){
                String sss = obj.toString();
                Gson gson = new Gson();
                Map<String, Object> map = new HashMap<String, Object>();
                map = gson.fromJson(sss, map.getClass());

                commonUse.setUserId(new Integer((String) map.get("userid")) );
                commonUse.setPlanterName((String) map.get("planterName"));
                commonUse.setTemperature((String) map.get("temperature"));
                commonUse.setEnvHumidity((String) map.get("envhumidity"));
                commonUse.setPh((String) map.get("ph"));
                commonUse.setLight((String) map.get("light"));
                commonUse.setSoilHumidity((String) map.get("soilhumidity"));
                commonUse.setFactoryName((String) map.get("factoryname"));
                commonUse.setQualityInspector((String) map.get("qualityinspector"));
                commonUse.setProcessPerson((String) map.get("processperson"));
                commonUse.setDate((String) map.get("date"));
                commonUse.setLocation((String) map.get("location"));
                commonUse.setInTime((String) map.get("intime"));
                commonUse.setOutTime((String) map.get("outtime"));
                commonUse.setTransportPerson((String) map.get("transportperson"));
                commonUse.setLeaveAddress((String) map.get("leaveaddress"));
                commonUse.setArriveAddress((String) map.get("arriveaddress"));
                commonUse.setMallName((String) map.get("mallname"));
                commonUse.setSaleTime((String) map.get("saletime"));
                commonUse.setPrice((String) map.get("price"));
                commonUse.setRole((String) map.get("role"));
                commonUses.add(commonUse);
            }else {
//                commonUse.setRole("管理员");
//                commonUse.setOrderstatus("已删除");
                commonUses.add(commonUse);
            }

        }
        for(int i = 0;i<commonUses.size();i++){
            System.out.println(commonUses.get(i));
        }
        return commonUses;
    }

}
